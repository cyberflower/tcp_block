all : clean tcp_block

tcp_block: myarp.o main.o
	g++ -g -o tcp_block main.o myarp.o -lpcap

main.o:
	g++ -std=c++14 -g -c -o main.o main.cpp

myarp.o:
	g++ -std=c++14 -g -c -o myarp.o myarp.cpp

clean:
	rm -rf *.o