#include "myarp.h"

void usage() {
    printf("syntax : tcp_block <interface> <host>\n");
    printf("sample : tcp_block en0 test.gilgil.net\n");
}

int main(int argc, char *argv[]){
    if (argc != 3) {
        usage();
        return -1;
    }
    char* dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
    if (handle == NULL) {
        fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
        return -1;
    }

    while(true){
        TCP_block tcp;
        struct pcap_pkthdr *header;
        const u_char *packet;
        int res=pcap_next_ex(handle, &header, &packet);
        if(res==-1 || res==-2) break;
        if(res==0) continue;

        ether_header *eth_hdr=(ether_header *)packet;
        //if(eth_hdr->ether_type!=ETHERTYPE_IP) continue;
        if(!tcp.isblock((unsigned char*)packet+sizeof(libnet_ethernet_hdr),(unsigned char*)argv[2])) continue; // continue if url is not same as argv[2]
        printf("[+] find block url\n");
        net_info src;
        net_info dst;
        memcpy(src.IP,eth_hdr->ether_shost,IP_LEN);
        memcpy(dst.IP,eth_hdr->ether_dhost,IP_LEN);

        struct libnet_ipv4_hdr *ip_hdr = (struct libnet_ipv4_hdr *)(packet + sizeof(struct libnet_ethernet_hdr));
        memcpy(src.MAC,&(ip_hdr->ip_src),MAC_LEN);
        memcpy(dst.MAC,&(ip_hdr->ip_dst),MAC_LEN);
        struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)((u_char *)ip_hdr + (ip_hdr->ip_hl) * 4);
        src.port=tcp_hdr->th_sport;
        dst.port=tcp_hdr->th_dport;
        u_char send_packet[200]={0,};
        //memcpy(send_packet,packet,sizeof(libnet_ethernet_hdr)+(ip_hdr->ip_hl) * 4+ (tcp_hdr->th_off) * 4);
        tcp.send_TCP_rst_packet(handle, send_packet, &dst, &src, tcp_hdr->th_ack, tcp_hdr->th_seq+4*tcp_hdr->th_off);
        tcp.send_TCP_rst_packet(handle, send_packet, &src, &dst, tcp_hdr->th_seq+4*tcp_hdr->th_off, tcp_hdr->th_ack);

        //tcp.send_TCP_fin_packet(handle, send_packet, &dst, &src, tcp_hdr->th_ack, tcp_hdr->th_seq+4*tcp_hdr->th_off);
        //tcp.send_TCP_fin_packet(handle, send_packet, &src, &dst, tcp_hdr->th_seq+4*tcp_hdr->th_off, tcp_hdr->th_ack);        
        printf("[+] send rst packet\n");
        //sleep(100);
    }

    return 0;
}