#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <string.h> 
#include <string>     
//#include <net/if_dl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <map>
#include <thread>
#include <libnet.h>

using namespace std;

#pragma comment(lib,"lphlpapi.lib")

#define IP_LEN 4
#define MAC_LEN 6

struct net_info{
  u_char MAC[MAC_LEN];
  u_char IP[IP_LEN];
  uint16_t port;
};

struct pseudo_hdr{
    struct in_addr saddr;
    struct in_addr daddr;
    uint8_t zero;
    uint8_t protocol;
    uint16_t length;
    struct libnet_tcp_hdr tcpheader;
};

struct TCP_block{
  bool isblock(unsigned char *data, unsigned char *cmp_data);
  void find_url(char *data, int data_len, int *start, int *len);
  uint16_t IP_checksum(u_char *buf, uint8_t len);
  uint16_t TCP_checksum(u_char *buf, uint8_t len);
  bool send_TCP_rst_packet(pcap_t* handle, u_char *packet, net_info *src, net_info *dst, uint32_t seq, uint32_t ack);
  bool send_TCP_fin_packet(pcap_t* handle, u_char *packet, net_info *src, net_info *dst, uint32_t seq, uint32_t ack);
};