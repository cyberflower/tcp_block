#include "myarp.h"

void TCP_block::find_url(char *data, int data_len, int *start, int *len){
    //printf("%d\n",data_len);
	char host[10]="Host: ";
	for(int i=0;i<data_len-6;i++){
		if(memcmp(data+i,host,strlen(host))==0){
			*start=i+6;
			break;
		}
	}
	*len=0;
	for(int i=(*start);i<data_len-2;i++){
		if(memcmp(data+i,"\x0d\x0a",2)==0){
			*len=(i-(*start));
			break;
		}
	}
}

bool TCP_block::isblock(unsigned char *data, unsigned char *cmp_data){
	char start_tcp_data[6][10]={"GET", "POST", "HEAD", "PUT", "DELETE", "OPTIONS"};
	struct libnet_ipv4_hdr *ipv4_hdr=(struct libnet_ipv4_hdr*)data;
	struct libnet_tcp_hdr *tcp_hdr=(struct libnet_tcp_hdr*)(data+ipv4_hdr->ip_hl*4);
	unsigned char *app_data=data+ipv4_hdr->ip_hl*4+tcp_hdr->th_off*4;
	if(ipv4_hdr->ip_v==0x4 && ipv4_hdr->ip_p==0x6){
		for(int i=0;i<6;i++){
			if(memcmp(start_tcp_data[i],app_data,strlen(start_tcp_data[i]))==0){
				int start_url, len;
				find_url((char*)app_data,100,&start_url,&len);
                //printf("[+] url: ");
                //printf("%s",app_data+start_url);
                //printf("\n");
				if(len==strlen((char*)cmp_data) && memcmp(app_data+start_url,cmp_data,len)==0) return true;
			}
		}
		return false;
	}
	return false;
}

//algorithm from https://websecurity.tistory.com/119
uint16_t TCP_block::IP_checksum(u_char *buf, uint8_t len){
    uint32_t sum=0;
    for(int i=0;i<len;i++){
        sum+=buf[i];
    }
    uint16_t res=(sum>>16)+(sum&0xffff);
    return ~res;
}

// algorithm from DAKA blog
uint16_t TCP_block::TCP_checksum(u_char *buf, uint8_t len){
    uint32_t res=0; 
    while(len--) res += *buf++;
    res = (res >> 16) + (res & 0xffff);
    res += (res >> 16);
    return (uint16_t)(~res);
}

bool TCP_block::send_TCP_rst_packet(pcap_t* handle, u_char *packet, net_info *src, net_info *dst, uint32_t seq, uint32_t ack){
    struct libnet_ethernet_hdr *eth_hdr = (struct libnet_ethernet_hdr *)packet;
    memcpy(eth_hdr->ether_shost, src->MAC, MAC_LEN);
    memcpy(eth_hdr->ether_dhost, dst->MAC, MAC_LEN);
    eth_hdr->ether_type=htons(ETHERTYPE_IP);

    struct libnet_ipv4_hdr *ip_hdr = (struct libnet_ipv4_hdr *)(packet + sizeof(libnet_ethernet_hdr));
    memcpy(&(ip_hdr->ip_src),src->IP,IP_LEN);
    memcpy(&(ip_hdr->ip_dst),dst->IP,IP_LEN);
    ip_hdr->ip_hl = 5;
    ip_hdr->ip_v = 4;
    ip_hdr->ip_ttl = 144;
    ip_hdr->ip_p = IPPROTO_TCP;
    ip_hdr->ip_len = htons(sizeof(struct libnet_ipv4_hdr) + sizeof(struct libnet_tcp_hdr));
    ip_hdr->ip_sum=htons(IP_checksum((u_char*)ip_hdr,ip_hdr->ip_hl*4));

    struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)((u_char *)ip_hdr + (ip_hdr->ip_hl) * 4);
    tcp_hdr->th_sport=htons(src->port);
    tcp_hdr->th_dport=htons(dst->port);
    tcp_hdr->th_off=5;

    tcp_hdr->th_sum=0;
    pseudo_hdr buf;
    buf.saddr = ip_hdr->ip_src;
    buf.daddr = ip_hdr->ip_dst;
    buf.protocol     = ip_hdr->ip_p;
    buf.length       = htons((uint16_t)sizeof(struct libnet_tcp_hdr));
    memcpy(&(buf.tcpheader), tcp_hdr, sizeof(struct libnet_tcp_hdr));

    tcp_hdr->th_sum=htons(TCP_checksum((u_char *)&buf,sizeof(struct pseudo_hdr)));
    tcp_hdr->th_flags = TH_RST; 
    if(pcap_sendpacket(handle, packet, sizeof(struct libnet_ethernet_hdr)+(ip_hdr->ip_hl)*4+(tcp_hdr->th_off)*4)==-1){
        printf("[-] Error while sending packet!\n");
        return false;
    }
    return true;
}

bool TCP_block::send_TCP_fin_packet(pcap_t* handle, u_char *packet, net_info *src, net_info *dst, uint32_t seq, uint32_t ack){
    struct libnet_ethernet_hdr *eth_hdr = (struct libnet_ethernet_hdr *)packet;
    memcpy(eth_hdr->ether_shost, src->MAC, MAC_LEN);
    memcpy(eth_hdr->ether_dhost, dst->MAC, MAC_LEN);
    eth_hdr->ether_type=htons(ETHERTYPE_IP);

    struct libnet_ipv4_hdr *ip_hdr = (struct libnet_ipv4_hdr *)(packet + sizeof(libnet_ethernet_hdr));
    memcpy(&(ip_hdr->ip_src),src->IP,IP_LEN);
    memcpy(&(ip_hdr->ip_dst),dst->IP,IP_LEN);
    ip_hdr->ip_hl = 5;
    ip_hdr->ip_v = 4;
    ip_hdr->ip_ttl = 144;
    ip_hdr->ip_p = IPPROTO_TCP;
    ip_hdr->ip_len = htons(sizeof(struct libnet_ipv4_hdr) + sizeof(struct libnet_tcp_hdr));
    ip_hdr->ip_sum=htons(IP_checksum((u_char*)ip_hdr,ip_hdr->ip_hl*4));

    struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)((u_char *)ip_hdr + (ip_hdr->ip_hl) * 4);
    tcp_hdr->th_sport=htons(src->port);
    tcp_hdr->th_dport=htons(dst->port);
    tcp_hdr->th_off=5;

    tcp_hdr->th_sum=0;
    pseudo_hdr buf;
    buf.saddr = ip_hdr->ip_src;
    buf.daddr = ip_hdr->ip_dst;
    buf.protocol     = ip_hdr->ip_p;
    buf.length       = htons((uint16_t)sizeof(struct libnet_tcp_hdr));
    memcpy(&(buf.tcpheader), tcp_hdr, sizeof(struct libnet_tcp_hdr));

    tcp_hdr->th_sum=htons(TCP_checksum((u_char *)&buf,sizeof(struct pseudo_hdr)));
    tcp_hdr->th_flags = TH_FIN; 
    if(pcap_sendpacket(handle, packet, sizeof(struct libnet_ethernet_hdr)+(ip_hdr->ip_hl)*4+(tcp_hdr->th_off)*4)==-1){
        printf("[-] Error while sending packet!\n");
        return false;
    }
    return true;
}